from lelang.models import *
from datetime import datetime
import pytz

def update_lelang():
    try:
        all_lelang = Lelang.objects.all()
        for lelang in all_lelang:
            if lelang.get_lelang_endtime() <= datetime.now(pytz.timezone("Asia/Bangkok")) and lelang.get_status() == "Open":
                transaksi = Transaksi(lelang=lelang)
                lelang.lelang_status = "Closed"
                transaksi.pengguna = lelang.get_highestbidder()
                lelang.save()
                transaksi.save()
    except:
        pass