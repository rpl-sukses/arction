from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from scheduler import lelangApi

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(lelangApi.update_lelang, 'interval', minutes=1)
    scheduler.start()