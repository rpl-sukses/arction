from django.shortcuts import render, redirect
from django.core.serializers import serialize
from profil.models import *
import re

# Create your views here.
def login(request):
    if request.method == "POST":
        username = request.POST['username'].strip().lower()
        password = request.POST['password']
        if len(username) == 0 or len(password) == 0 or " " in username:
            ## If form not valid
            dct = {
                'message' : 'form_not_valid'
            }
            return render(request, 'authentication/login.html', dct)
        else:
            try:
                user = Akun.objects.get(username=username)
                password_is_correct = password_checker(password, user.password)
                if (password_is_correct):
                    request.session['username'] = username
                    role = []
                    is_pengguna = check_is_pengguna(username)
                    is_penjual = check_is_penjual(username)
                    is_admin = check_is_admin(username)
                    if is_pengguna:
                        role.append('pengguna')
                    if is_penjual:
                        user = Penjual.objects.get(username=username)
                        role.append('penjual')
                    
                    if is_admin:
                        role.append('admin')
                    request.session['role'] = role
                    return redirect('/')
                else:
                    ## If wrong password
                    dct = {
                        'message' : 'failed_auth'
                    }
                    return render(request, 'authentication/login.html', dct)
            except:
                ## If user account does not exist
                dct = {
                    'message' : 'not_exist'
                }
                return render(request, 'authentication/login.html', dct)
    else:
        return render(request, 'authentication/login.html')

def check_is_admin(username):
    count = Admin.objects.filter(username=username)
    return len(count) == 1

def check_is_pengguna(username):
    count = Pengguna.objects.filter(username=username)
    return len(count) == 1

def check_is_penjual(username):
    count = Penjual.objects.filter(username=username)
    return len(count) == 1

def password_checker(input_password, real_password):
    return input_password == real_password

def register(request):
    if request.method == "POST":
        nama = request.POST['nama']
        email = request.POST['email'].strip().lower()
        username = request.POST['username'].strip().lower()
        password = request.POST['password']
        if len(nama) == 0 or len(email) == 0 or len(username) < 6 or len(password) < 6 or " " in username or not validate_email(email):
            dct = {
                    'message' : 'form_not_valid'
                }
            return render(request, 'authentication/register.html', dct)
            
        else:
            username_is_exist = username_exist_checker(username)
            if (username_is_exist == False):
                user = Pengguna.objects.create(username=username, password=password, nama=nama, email=email, saldo=0)
                request.session['username'] = username
                request.session['role'] = ['pengguna']
                return redirect('/')
            else:
                ## If user account already exist
                dct = {
                    'message' : 'exist'
                }
                return render(request, 'authentication/register.html', dct)
    else:
        return render(request, 'authentication/register.html')

def username_exist_checker(username):
    count = Akun.objects.filter(username=username)
    return len(count) == 1

def validate_email(email):
    if re.match('^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+([.]\w+)+$', email):
        return True
    else:
        return False

def daftar_sebagai_penjual(request):
    try:
        username = request.session['username']
        if len(username) == 0:
            return redirect("/")
        if 'penjual' in request.session['role'] or check_is_penjual(username):
            return redirect("/")
    except:
        return redirect("/")
       
    if request.method == "POST":
        username = request.session['username']
        nomor_identitas = request.POST['nomor_identitas']
        if len(nomor_identitas) < 10:
            response = {
                'message' : 'form_not_valid'
            }
            return render(request, 'authentication/seller.html', response)
        else:
            response = {}
            if check_request_penjual_exist(username):
                return redirect("/")
            req = RequestPenjual.objects.create(
                username=username, 
                status='menunggu persetujuan',
                nomor_identitas=nomor_identitas
            )
            response["status_request"] = 'menunggu persetujuan'
            return render(request, 'authentication/sellerrespond.html', response)
    else:
        username = request.session['username']
        if check_request_penjual_exist(username):
            res = daftar_sebagai_penjual_respond(request)
            return res
        return render(request, 'authentication/seller.html')

def daftar_sebagai_penjual_respond(request):
    # check not penjual role
    try:
        username = request.session['username']
        if len(username) == 0:
            return redirect("/")
        if 'penjual' in request.session['role'] or check_is_penjual(username):
            return redirect("/")
    except:
        return redirect("/")
    
    username = request.session['username']
    if not check_request_penjual_exist(username):
        return render(request, 'authentication/seller.html')

    response = {}
    req = RequestPenjual.objects.get(username=username)
    response["status_request"] = req.status
    if req.status == "ditolak":
        req.delete()
    return render(request, 'authentication/sellerrespond.html', response)

def check_request_penjual_exist(username):
    count = RequestPenjual.objects.filter(username=username)
    return len(count) == 1

def admin_validation_page(request):
    response = {}
    # check admin role
    try:
        username = request.session['username']
        if len(username) == 0:
            return redirect("/")
        if 'admin' not in request.session['role'] or not check_is_admin(username):
            return redirect("/")
    except:
        return redirect("/")

    req_list = RequestPenjual.objects.filter(status='menunggu persetujuan')
    response['req_list'] = req_list 
    return render(request, 'authentication/adminvalidation.html', response)

def admin_validation_accept(request, id):
    response = {}
    try:
        req = RequestPenjual.objects.get(id=id)
        print(req.username)
        pengguna = Pengguna.objects.get(username=req.username)
        print(pengguna)
        penjual = Penjual(
            username=pengguna.username, 
            password=pengguna.password, 
            nama=pengguna.nama, 
            email=pengguna.email, 
            saldo=0,
            pengguna_ptr=pengguna, 
            nomor_identitas=req.nomor_identitas,
        )
        print(penjual)
        penjual.save()
        flag = True

        req.status = "diterima"
        req.save()
        response['message'] = "success"
    except:
        response['message'] = "failed"
    
    req_list = RequestPenjual.objects.filter(status='menunggu persetujuan')
    response['req_list'] = req_list 
    return render(request, 'authentication/adminvalidation.html', response)

def admin_validation_decline(request, id):
    response = {}
    try:
        req = RequestPenjual.objects.get(id=id)
        req.status = "ditolak"
        req.save()
        response['message'] = "success"
    except:
        response['message'] = "failed"
    req_list = RequestPenjual.objects.filter(status='menunggu persetujuan')
    response['req_list'] = req_list 
    return render(request, 'authentication/adminvalidation.html', response)


def logout(request):
    request.session.flush()
    return redirect('/')