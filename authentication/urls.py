from django.urls import path
from .views import *

app_name = "authentication"

urlpatterns = [
    path('login/', login, name="login"),
    path('register/', register, name="register"),
    path('logout/', logout, name="logout"),
    path('register-penjual/', daftar_sebagai_penjual, name="register-penjual"),
    path('register-penjual/respond', daftar_sebagai_penjual_respond, name="register-penjual-respond"),
    path('register-penjual/validation', admin_validation_page, name="admin-validation"),
    path('register-penjual/validation/accept/<int:id>', admin_validation_accept, name="admin-validation-accept"),
    path('register-penjual/validation/decline/<int:id>', admin_validation_decline, name="admin-validation-decline"),
]