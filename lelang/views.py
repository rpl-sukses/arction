from django.shortcuts import render,redirect
from profil.models import *
from .models import *
from datetime import datetime, timedelta
from django.contrib import messages
from django.http import HttpResponseNotFound
from django.core.exceptions import ObjectDoesNotExist
import pytz

# Create your views here.
def create(request):
    if request.method == "POST":
        barang_nama = request.POST['barang_nama']
        barang_deskripsi = request.POST['barang_deskripsi']
        barang_url = request.POST['barang_url']
        lelang_startingbid = request.POST['lelang_startingbid']
        lelang_buyoutprice = request.POST['lelang_buyoutprice']
        lelang_minraise = request.POST['lelang_minraise']
        lelang_starttime = request.POST['lelang_starttime']
        lelang_endtime = request.POST['lelang_endtime']
        if (is_datetime_correct(lelang_starttime, lelang_endtime)):
            lelang_penjual = Penjual.objects.get(username=request.session['username'])
            barang = Barang.objects.create(barang_nama=barang_nama, barang_deskripsi=barang_deskripsi, barang_url=barang_url)
            lelang = Lelang.objects.create(lelang_penjual=lelang_penjual, lelang_barang=barang, lelang_startingbid=lelang_startingbid, lelang_buyout=lelang_buyoutprice,
                lelang_minraise=lelang_minraise, lelang_currentbid=lelang_startingbid, lelang_starttime=lelang_starttime, lelang_endtime=lelang_endtime)
            return redirect('/profil/lelang-dibuat')
        else :
            messages.info(request, "Input tanggal dan waktu yang diberikan tidak tepat!")
            return redirect(request.path_info)
    else:
        return render(request, 'lelang/create.html')

def is_datetime_correct(start, end):
    date_start = datetime.strptime(start, '%Y-%m-%dT%H:%M')
    date_end = datetime.strptime(end, '%Y-%m-%dT%H:%M')
    if (date_start < date_end and date_start > datetime.now()):
        return True
    return False

def update(request, id):
    try:
        if request.method == "POST":
            lelang = Lelang.objects.get(pk=id)
            lelang.lelang_barang.barang_nama = request.POST['barang_nama']
            lelang.lelang_barang.barang_deskripsi = request.POST['barang_deskripsi']
            lelang.lelang_barang.barang_url = request.POST['barang_url']
            lelang.lelang_startingbid = request.POST['lelang_startingbid']
            lelang.lelang_buyoutprice = request.POST['lelang_buyoutprice']
            lelang.lelang_minraise = request.POST['lelang_minraise']
            lelang.lelang_starttime = request.POST['lelang_starttime']
            lelang.lelang_endtime = request.POST['lelang_endtime']
            lelang.lelang_barang.save()
            lelang.save()
            return redirect('/profil/lelang-dibuat')
        else:
            lelang = Lelang.objects.get(pk=id)
            if (lelang.lelang_starttime - datetime.now(pytz.timezone('Asia/Bangkok')) <= timedelta(0)):
                messages.info(request, 'Lelang tidak dapat diedit dikarenakan lelang sudah dimulai')
            dct = {
                'dct_barang_nama' : lelang.lelang_barang.barang_nama,
                'dct_barang_deskripsi' : lelang.lelang_barang.barang_deskripsi,
                'dct_barang_url' : lelang.lelang_barang.barang_url,
                'dct_lelang_startingbid' : lelang.lelang_startingbid,
                'dct_lelang_buyoutprice' : lelang.lelang_buyout,
                'dct_lelang_minraise' : lelang.lelang_minraise,
                'dct_lelang_starttime' : lelang.lelang_starttime,
                'dct_lelang_endtime' : lelang.lelang_endtime,
            }
            return render(request, 'lelang/update.html', dct)
    except ObjectDoesNotExist:
        return HttpResponseNotFound("OBJECT DOES NOT EXIST")
            
       
def createtest(request):
    if request.method == "POST":
        pass
    else:
        return render(request, 'lelang/createtest.html')

def updatetest(request):
    if request.method == "POST":
        pass
    else:
        return render(request, 'lelang/updatetest.html')
        
def each_lelang(request, id):
    lelang = Lelang.objects.get(pk=id)
    username_penjual = lelang.get_penjual().username
    dct = {
        'lelang' : lelang,
        'owner_username' : username_penjual
    }
    if lelang.get_lelang_starttime() > datetime.now(pytz.timezone('Asia/Bangkok')):
        dct['editable'] = False
        if (lelang.get_status() == "Soon"):
            lelang.lelang_status = "Open"
            lelang.save()
    if lelang.get_lelang_endtime() < datetime.now(pytz.timezone('Asia/Bangkok')):
        dct['show_winner'] = True
    else:
        dct['editable'] = True
    return render(request, 'lelang/each_lelang.html', dct)

def raise_bid(request, id):
    authenticated = request.session.__contains__('username')
    if (authenticated):
        lelang = Lelang.objects.get(pk=id)

        is_penjual = request.session['username'] == lelang.get_username_penjual()
        if (is_penjual):
            return HttpResponseNotFound("404 NOT FOUND")
        else:

            # Kalau lelang belum dimulai
            if lelang.get_lelang_starttime() > datetime.now(pytz.timezone('Asia/Bangkok')):
                messages.info(request, "Belum dapat melakukan raise bid ataupun buyout untuk sementara waktu!")
                return redirect('lelang:each_lelang', id=id)

            # Kalau lelang telah berakhir
            if lelang.get_lelang_endtime() < datetime.now(pytz.timezone('Asia/Bangkok')):
                messages.info(request, "Pelelangan telah berakhir!")
                return redirect('lelang:each_lelang', id=id)
            
            # Kalau lelang sedang berjalan
            else:
                current_bid_price = lelang.get_currentbid_price()
                raise_bid_price = current_bid_price + lelang.get_minraise_price()
                user = Pengguna.objects.get(username=request.session['username'])
                current_saldo_user = user.get_saldo()
                old_highest_bidder = lelang.get_highestbidder()

                # Hanya bisa melakukan raise bid jika highest bidder sebelumnya bukan dirinya sendiri
                if user != old_highest_bidder:

                    # Check apakah saldo user cukup untuk raise bid
                    if (current_saldo_user >= raise_bid_price):
                        if lelang.get_highestbidder() is not None:
                            old_highest_bidder.set_saldo(old_highest_bidder.get_saldo() + current_bid_price)
                            old_highest_bidder.save()

                        user.set_saldo(user.get_saldo() - raise_bid_price)
                        user.save()

                        lelang.lelang_highestbidder = user
                        lelang.lelang_listpengguna.add(user)
                        lelang.lelang_currentbid = raise_bid_price

                        # Jika current bid terbaru melebihi atau sama dengan harga buyout
                        if lelang.get_currentbid_price() >= lelang.get_buyout_price():
                            lelang.lelang_endtime = datetime.now(pytz.timezone('Asia/Bangkok'))

                        lelang.save()
                        return redirect("lelang:each_lelang", id=id)
                    
                    # Jika saldo user yang melakukan raise bid tidak cukup
                    else:
                        messages.info(request, "Maaf, saldo anda tidak cukup!")
                        return redirect("lelang:each_lelang", id=id)
                
                # Jika raise bid yang highest bidder sebelumnya dirinya sendiri
                else:
                    messages.info(request, "Tidak dapat melakukan raise bid jika anda sebagai highest bidder")
                    return redirect("lelang:each_lelang", id=id)
    else:
        return HttpResponseNotFound("404 NOT FOUND")

def buyout(request, id):
    authenticated = request.session.__contains__('username')
    if (authenticated):
        lelang = Lelang.objects.get(pk=id)

        is_penjual = request.session['username'] == lelang.get_username_penjual()
        if (is_penjual):
            return HttpResponseNotFound("404 NOT FOUND")
        else:

            # Kalau lelang belum dimulai
            if lelang.get_lelang_starttime() > datetime.now(pytz.timezone('Asia/Bangkok')):
                messages.info(request, "Belum dapat melakukan raise bid ataupun buyout untuk sementara waktu!")
                return redirect('lelang:each_lelang', id=id)

            # Kalau lelang telah berakhir
            if lelang.get_lelang_endtime() < datetime.now(pytz.timezone('Asia/Bangkok')):
                messages.info(request, "Pelelangan telah berakhir!")
                return redirect('lelang:each_lelang', id=id)
            
            # Kalau lelang sedang berjalan
            else:
                current_bid_price = lelang.get_currentbid_price()
                buyout_price = lelang.get_buyout_price()
                user = Pengguna.objects.get(username=request.session['username'])
                current_saldo_user = user.get_saldo()
                old_highest_bidder = lelang.get_highestbidder()
                
                # Hanya bisa melakukan buyout jika highest bidder sebelumnya bukan dirinya sendiri
                if user != old_highest_bidder:
                    
                    # Check apakah saldo user cukup untuk buyout
                    if (current_saldo_user >= buyout_price):
                        if lelang.get_highestbidder() is not None:
                            old_highest_bidder.set_saldo(old_highest_bidder.get_saldo() + current_bid_price)
                            old_highest_bidder.save()

                        user.set_saldo(user.get_saldo() - buyout_price)
                        user.save()

                        lelang.lelang_highestbidder = user
                        lelang.lelang_listpengguna.add(user)
                        lelang.lelang_currentbid = buyout_price
                        lelang.lelang_endtime = datetime.now(pytz.timezone('Asia/Bangkok'))
                        lelang.save()

                        return redirect('lelang:each_lelang', id=id)

                    # Jika saldo user tidak cukup
                    else:
                        messages.info(request, "Maaf, saldo anda tidak cukup!")
                        return redirect("lelang:each_lelang", id=id)

                # Jika highest bidder sebelumnya adalah dirinya sendiri
                else:
                    messages.info(request, "Tidak dapat melakukan buyout jika anda sebagai highest bidder")
                    return redirect("lelang:each_lelang", id=id)
    else:
        return HttpResponseNotFound("404 NOT FOUND")

def list_lelang(request):
    response = {}
    lst = Lelang.objects.filter(
            lelang_starttime__lte=datetime.now(pytz.timezone('Asia/Bangkok'))
        ).filter(
            lelang_endtime__gt=datetime.now(pytz.timezone('Asia/Bangkok'))
        )
    response["list_lelang"] = lst
    return render(request, 'lelang/list_lelang.html', response)

