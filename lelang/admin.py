from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Barang)
admin.site.register(Lelang)
admin.site.register(Transaksi)