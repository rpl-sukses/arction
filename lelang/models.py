from django.db import models
from profil.models import *
from datetime import datetime

# Create your models here.

class Barang(models.Model):
    barang_nama = models.CharField(max_length=30)
    barang_deskripsi = models.TextField()
    barang_url = models.URLField()

    def __str__(self):
        return self.barang_nama
    
    def get_nama_barang(self):
        return self.barang_nama

    def get_deskripsi_barang(self):
        return self.barang_deskripsi
    
    def get_url_barang(self):
        return self.barang_url

class Lelang(models.Model):
    lelang_penjual = models.ForeignKey(Penjual, on_delete=models.DO_NOTHING)
    lelang_barang = models.ForeignKey(Barang, on_delete=models.DO_NOTHING)
    lelang_startingbid = models.IntegerField()
    lelang_buyout = models.IntegerField()
    lelang_minraise = models.IntegerField()
    lelang_currentbid = models.IntegerField(default=0)
    lelang_starttime = models.DateTimeField()
    lelang_endtime = models.DateTimeField()
    lelang_highestbidder = models.ForeignKey(Pengguna,null=True, on_delete=models.DO_NOTHING, related_name='highestbidder')
    lelang_listpengguna = models.ManyToManyField(Pengguna,related_name='follower')

    # Status List = ['Soon', 'Open', 'Closed']
    lelang_status = models.CharField(max_length=10, default='Soon')

    def __str__(self):
        return self.lelang_barang.__str__()

    def get_penjual(self):
        return self.lelang_penjual
    
    def get_username_penjual(self):
        return self.lelang_penjual.username
    
    def get_nama_barang(self):
        barang = self.lelang_barang
        return barang.get_nama_barang()
    
    def get_deskripsi_barang(self):
        barang = self.lelang_barang
        return barang.get_deskripsi_barang()

    def get_url_barang(self):
        barang = self.lelang_barang
        return barang.get_url_barang()
    
    def get_startingbid_price(self):
        return self.lelang_startingbid

    def get_buyout_price(self):
        return self.lelang_buyout
    
    def get_minraise_price(self):
        return self.lelang_minraise

    def get_currentbid_price(self):
        return self.lelang_currentbid
    
    def get_lelang_starttime(self):
        return self.lelang_starttime

    def get_lelang_endtime(self):
        return self.lelang_endtime
    
    def get_highestbidder(self):
        return self.lelang_highestbidder
    
    def get_listpengguna(self):
        return self.lelang_listpengguna
    
    def get_status(self):
        return self.lelang_status

class Transaksi(models.Model):
    lelang = models.ForeignKey(Lelang, on_delete=models.DO_NOTHING)
    status = models.CharField(max_length=10, default="Paid")
    pengguna = models.ForeignKey(Pengguna, on_delete=models.DO_NOTHING)
    tanggal_terbuat = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.lelang.get_nama_barang()

    def get_lelang(self):
        return self.lelang
    
    def get_status(self):
        return self.status

    def get_pengguna(self):
        return self.pengguna

    def get_currentbid_price(self):
        return self.lelang.get_currentbid_price()

    def get_nama_barang(self):
        return self.lelang.get_nama_barang()

    def get_url_image(self):
        return self.lelang.get_url_barang()

    def get_tanggal_terbuat(self):
        return self.tanggal_terbuat