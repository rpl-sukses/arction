from django.urls import path
from .views import *

app_name = "lelang"

urlpatterns = [
    path('create/', create, name="create"),
    path('update/<int:id>', update, name="update"),
    path('createtest/', createtest, name="createtest"),
    path('updatetest/', updatetest, name="updatetest"),
    path('<int:id>', each_lelang, name="each_lelang"),
    path('', list_lelang, name="list_lelang"),
    path('raisebid/<int:id>', raise_bid, name="raisebid"),
    path('buyout/<int:id>', buyout, name="buyout")
]