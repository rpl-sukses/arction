from django.apps import AppConfig


class LelangConfig(AppConfig):
    name = 'lelang'

    def ready(self):
        from scheduler import update
        update.start()