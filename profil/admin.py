from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Pengguna)
admin.site.register(Penjual)
admin.site.register(Akun)
admin.site.register(Admin)
admin.site.register(Request)
admin.site.register(RequestPenjual)