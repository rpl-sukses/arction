from django.shortcuts import render
from lelang.models import *
from datetime import datetime, timedelta
import pytz
from django.http import HttpResponseNotFound

# Create your views here.
def history_terbeli(request):
    authenticated = request.session.__contains__('username')
    if (authenticated):
        pengguna = Pengguna.objects.get(username=request.session['username'])
        transaksi = Transaksi.objects.filter(pengguna=pengguna)
        dct = {
            "transaksi" : transaksi
        }
        return render(request, 'profil/daftar_barang_terbeli.html',  dct)
    else:
        return HttpResponseNotFound("404 Not Found")

def history_terjual(request):
    authenticated = request.session.__contains__('username')
    if (authenticated):
        if (Penjual.objects.filter(username=request.session['username'])):
            penjual = Penjual.objects.get(username=request.session['username'])
            lelang_penjual = Lelang.objects.filter(lelang_penjual=penjual)
            lelang_berhasil = []
            for objek in lelang_penjual:
                if(objek.lelang_highestbidder is not None and 
                objek.get_lelang_endtime() < datetime.now(pytz.timezone('Asia/Bangkok'))):
                    lelang_berhasil.append(objek)
            dct = {
                'lelang_berhasil' : lelang_berhasil,
            }
            return render(request, 'profil/daftar_barang_terjual.html', dct)
        else:
            return HttpResponseNotFound("404 NOT FOUND")
    else:
        return HttpResponseNotFound("404 NOT FOUND")


def lelang_dibuat(request):
    authenticated = request.session.__contains__('username')
    if (authenticated):
        if (Penjual.objects.filter(username=request.session['username'])):
            penjual = Penjual.objects.get(username=request.session['username'])
            lelang = Lelang.objects.filter(lelang_penjual=penjual)
            lelang_with_status = []
            for objek in lelang:
                if (objek.lelang_starttime - datetime.now(pytz.timezone('Asia/Bangkok')) > timedelta(0)):
                    lelang_with_status.append([objek, True])
                else:
                    lelang_with_status.append([objek, False])
            dct = {
                'lst' : lelang_with_status,
                'time' : datetime.now(),
            }
            return render(request, 'profil/daftar_lelang_dibuat.html', dct)
        else:
            return HttpResponseNotFound("404 NOT FOUND")
    else:
        return HttpResponseNotFound("404 NOT FOUND")

def lelang_diikuti(request):
    return render(request, 'profil/daftar_lelang_diikuti.html')