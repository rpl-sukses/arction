from django.db import models

# Create your models here.
class Akun(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.username

class Admin(Akun):

    def __str__(self):
        return super().__str__()

class Pengguna(Akun):
    email = models.EmailField(max_length=50)
    nama = models.CharField(max_length=100)
    saldo = models.IntegerField()

    def __str__(self):
        return self.nama
    
    def set_saldo(self, new_saldo):
        self.saldo = new_saldo
    
    def get_saldo(self):
        return self.saldo

class Penjual(Pengguna):
    nomor_identitas = models.CharField(max_length=50)

    def __str__(self):
        return super().__str__()

class Request(models.Model):
    username = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    
    def __str__(self):
        return self.username

class RequestPenjual(Request):
    nomor_identitas = models.CharField(max_length=50)

    def __str__(self):
        return super().__str__()