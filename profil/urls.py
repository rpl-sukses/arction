from django.urls import path
from .views import *

app_name = "profil"

urlpatterns = [
    path('history-terbeli/', history_terbeli, name="history_terbeli"),
    path('history-terjual/', history_terjual, name="history_terjual"),
    path('lelang-dibuat/', lelang_dibuat, name="lelang_dibuat"),
    path('lelang-diikuti/', lelang_diikuti, name="lelang_diikuti"),
]